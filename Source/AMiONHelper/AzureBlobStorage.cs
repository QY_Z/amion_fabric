﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.IO;

namespace AMiON.Helper
{
    public class AzureBlobStorage
    {
        string containerName = "mappingfiles";
        string StorageConnection = "DefaultEndpointsProtocol=https;AccountName=amiondevstorage;AccountKey=akz86upqM7IDThFeTHazuD1Y26sKTEPEME0CLlmVD/HptrS5J0RZPCFM1eLOIbTcgTUSSU0xCXqXeq+ME17/tQ==;EndpointSuffix=core.windows.net";

        public void UploadFileToBlobStorage(Stream fileStream, string fileName, string tenantId, string internalTeamId)
        {
            var filePath = $"Amion/{tenantId}/{internalTeamId}/{fileName}";
            
            var blockBlob = GetBlobInstance().GetBlockBlobReference(filePath);
            blockBlob.UploadFromStream(fileStream);
        }

        public Stream DownloadFileFromBlobStorage(string fileName, string tenantId, string internalTeamId)
        {
            var filePath = $"Amion/{tenantId}/{internalTeamId}/{fileName}";
            var reader = new MemoryStream();
            GetBlobInstance().GetBlockBlobReference(filePath).DownloadToStreamAsync(reader).GetAwaiter().GetResult();
            return reader;
        }

        private CloudBlobContainer GetBlobInstance()
        {
            var storageAccount = CloudStorageAccount.Parse(StorageConnection);
            var myClient = storageAccount.CreateCloudBlobClient();
            var container = myClient.GetContainerReference(containerName);
            container.CreateIfNotExists(BlobContainerPublicAccessType.Blob);
            return container;
        }
    }
}
