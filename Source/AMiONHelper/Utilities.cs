﻿using ExcelDataReader;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMiON.Helper
{
    public class Utilities
    {
        public static DataRow CheckValueExistInDatatableBasedonColumnNumber(int columnNumber, string valueWhichNeedsCheckedinColumn, DataTable dt) => dt.AsEnumerable().Where(c => c.Field<string>(columnNumber) == valueWhichNeedsCheckedinColumn)?.First();

        public static string GetColumnValueIfExist(DataRow rec, string columnName) => rec.Table.Columns.Contains(columnName) ? rec.Field<string>(columnName) ?? string.Empty : string.Empty;

        //IEnumerable<T> JoinedTeamsResponseParse<T>(JToken jToken, List<TeamList> teamList)
        //{
        //    //var result = JsonConvert.DeserializeObject<IEnumerable<T>>(jToken["body"]["value"].ToString(), Converter.Settings);
        //    //var teamID = jToken["id"].ToString().Split('#')[1];
        //    //foreach (dynamic item in result)
        //    //    item.TeamID = teamID;
        //    //teamList.Where(team => team..ToString() == teamID).First().nextLink = jToken["body"]["@odata.nextLink"] == null ? null : jToken["body"]["@odata.nextLink"].ToString();


        //    //return result;
        //}

        /// <summary>
        /// Method to convert stream object to datatable
        /// </summary>
        /// <param name="stream">Input stream object</param>
        /// <returns>Returns a data table</returns>
        public static DataTable ConvertExcelStreamDataToDataTable(Stream stream)
        {
            //checking input object is null
            if (stream == null)
            {
                throw new ArgumentNullException(nameof(stream));
            }

            var ShiftDetailsDataTable = new DataTable();
            try
            {
                // We return the interface, so that
                IExcelDataReader reader = null;

                //Creating instance of ExcelDataReader
                reader = ExcelReaderFactory.CreateReader(stream);
                    
                //Setting the first row as header
                var conf = new ExcelDataSetConfiguration
                {
                    ConfigureDataTable = _ => new ExcelDataTableConfiguration
                    {
                        UseHeaderRow = true
                    }
                };
                DataSet result = reader.AsDataSet(conf);
                reader.Close();
                if (result != null)
                {
                    return result.Tables[0];
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        /// <summary>
        /// Method which takes data table as object and converts to List MappingModel object
        /// </summary>
        /// <param name="mappingModelDataTable"></param>
        /// <returns>List of Mapping model object</returns>
        public static List<MappingModel> ConvertMappingDataTableToListMappingModel(DataTable mappingModelDataTable)
        {
            if (mappingModelDataTable == null)
            {
                throw new ArgumentNullException(nameof(mappingModelDataTable));
            }

            var lstAmionMappingModel = new List<MappingModel>();
            // trim column names
            foreach (DataColumn dc in mappingModelDataTable.Columns) 
            {
                dc.ColumnName = dc.ColumnName.Trim();
            }
            //Reading the data table and mapping to model object
            lstAmionMappingModel.AddRange(mappingModelDataTable.AsEnumerable().Select(rec => new MappingModel()
            {
                AMiONDivision = GetColumnValueIfExist(rec, "AMiONDivision"),
                TeamsTeam = GetColumnValueIfExist(rec, "TeamsTeam"),
                AMiONAssignment = GetColumnValueIfExist(rec, "AMiONAssignment"),
                ShiftGroup_Role = GetColumnValueIfExist(rec, "ShiftGroup_Role"),
                ShouldImport = GetColumnValueIfExist(rec, "Import")
            }));
            return lstAmionMappingModel;
        }
    }
}
