﻿using System;
using System.IO;

namespace AMiON.Helper
{
    public class AmiOnDataProvider
    {
        //private static string _amiOnUrl = @"http://www.amion.com/cgi-bin/ocs?Lo={0}&Rpt=625tabs--&Day={1}&Month={2}&Year=2019";
        private static string _amiOnUrl = @"http://www.amion.com/cgi-bin/ocs?Lo={0}&Rpt=625btabs--&Month={1}&Day={2}&Year={3}&Days={4}";
        
        public static Stream GetAmionData(string amionLogin)
        {
            string amiOnUrl = string.Format(_amiOnUrl,
                                            amionLogin,
                                            DateTime.Now.Day,
                                            DateTime.Now.Month,
                                            DateTime.Now.Year,
                                            System.Configuration.ConfigurationManager.AppSettings["DaysCount"].ToString());
            return RestHelper.GetDataFromUrl(amiOnUrl) == null ? null : RestHelper.GetDataFromUrl(amiOnUrl).GetAwaiter().GetResult();
        }
    }
}
