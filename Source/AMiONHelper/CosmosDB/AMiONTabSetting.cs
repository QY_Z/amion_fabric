﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMiON.Helper.CosmosDB
{

    public class AMiONTabSetting
    {
        public string DB = "user-settings-db";
        public string AdminTabConfigurationDetailCollectionName = "AdminTabConfigurationDetailCollection";
        public string UserSettingCollection = "UserSettingCollection";


        public AdminTabConfigurationDetails GetAdminTabConfigurationDetails(string TeamId, string ChannelId)
        {
            return CosmosDB<AdminTabConfigurationDetails>.GetItemAsync(x => x.TeamId.ToString() == TeamId.ToString() && x.ChannelId.ToString() == ChannelId.ToString(), DB, AdminTabConfigurationDetailCollectionName);
        }

        public UserSetting GetUserSetting(string UserID, string TeamId, string ChannelId)
        {
            return CosmosDB<UserSetting>.GetItemAsync(x => x.UserId.ToString() == UserID.ToString() && x.TeamId.ToString() == TeamId.ToString() && x.ChannelId.ToString() == ChannelId.ToString(), DB, UserSettingCollection);
        }

        public string InsertOrUpdateAdminTabConfigurationDetailCollection(AdminTabConfigurationDetails adminTabConfigurationDetails)
        {
            return CosmosDB<AdminTabConfigurationDetails>.InsertOrUpdateCollection(DB, AdminTabConfigurationDetailCollectionName, adminTabConfigurationDetails);
        }

        public string InsertOrUpdateUserSetting(UserSetting userSetting)
        {
            return CosmosDB<UserSetting>.InsertOrUpdateCollection(DB, UserSettingCollection, userSetting);
        }
    }

    public class AdminTabConfigurationDetails : IEntityDAO
    {
        public AdminTabConfigurationDetails()
        {
            Id = Guid.NewGuid();
        }
        public Guid TenantId { get; set; }
        public Guid TeamId { get; set; }
        public string ChannelId { get; set; }
        public string Login { get; set; }
        public string MappingFilePath { get; set; }
        public string[] ExcludedColumns { get; set; }
        public string[] DefaultColumnOrder { get; set; }
        public string[] SelectedDepartments { get; set; }
        public Guid Id { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string ETag { get; set; }
        public DateTime? CreationDate { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public Guid LastModifiedBy { get; set; }
    }

    public class UserSetting : IEntityDAO
    {
        public UserSetting()
        {
            Id = Guid.NewGuid();
        }

        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid TeamId { get; set; }
        public string ChannelId { get; set; }
        public Guid UserId { get; set; }
        public string[] ColumnsOrder { get; set; }
        public DateTime? CreationDate { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string ETag { get; set; }
    }
}
