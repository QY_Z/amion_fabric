﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using Microsoft.Extensions.DependencyInjection;

namespace AMiON.Helper.CosmosDB
{
    public static  class CosmosDB<T> where   T : class, IEntityDAO
    {

        private static  DocumentClient GetInstance()
        {
            var cosmosURL = "https://amiondev.documents.azure.com:443/";
            var cosmosAuthKey = "83peZYmtUbzc3Q9WTG1Xl5JspABwbPbdTjcPTSysFQhmVEqt9vFvwrj8H6SfR3YKAU8u0nahKIZaFWtQ3RWxKA==";

            var connectionPolicy = new ConnectionPolicy
            {
                ConnectionProtocol = Protocol.Tcp,
                ConnectionMode = ConnectionMode.Direct,
                MaxConnectionLimit = 1000,
                RequestTimeout = new TimeSpan(1, 0, 0),
                RetryOptions = new RetryOptions { MaxRetryWaitTimeInSeconds = 60, MaxRetryAttemptsOnThrottledRequests = 10 }
            };

            return new DocumentClient(new Uri(cosmosURL), cosmosAuthKey, connectionPolicy);

        }

        public  static string InsertOrUpdateCollection(string databaseName, string collectionName, object item)
        {
            try
            {
                Document doc = GetInstance().UpsertDocumentAsync(UriFactory.CreateDocumentCollectionUri(databaseName, collectionName), item).GetAwaiter().GetResult();
                return doc.Id;
            }
            catch (DocumentClientException e)
            {
                throw;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public static  T GetItemAsync(Expression<Func<T, bool>> where, string databaseName, string collectionName) 
        {
            try
            {
                IDocumentQuery<T> query = GetInstance().CreateDocumentQuery<T>(
                    UriFactory.CreateDocumentCollectionUri(databaseName, collectionName),
                    new FeedOptions { MaxItemCount = -1 })
                    .Where(where).AsDocumentQuery();

                return  query.ExecuteNextAsync<T>().GetAwaiter().GetResult().FirstOrDefault();
            }
            catch (DocumentClientException e)
            {
                throw;
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}
