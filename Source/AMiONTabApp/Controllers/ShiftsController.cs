﻿using AMiON.Helper;
using AMiON.Helper.CosmosDB;
using AMiONGraphShift.Shared;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Web.Mvc;

namespace AMiONGraphShift.Controllers
{
    public class ShiftsController : ControllerBaseHelper
    {
        [HttpGet]
        public ActionResult Auth()
        {
            return View();
        }

        string TeamId = "d442bd60-005e-4801-b5c4-d01b2c71c30f";
        string ChannelId = "19:5e22d9e2b79842aab19ffc0202809061@thread.skype";
        string UserID = "3ef0ebf1-7c44-4952-897c-f7725687273a";
        string TenantId = "f2d71ad2-df67-4e56-8c97-66411b4db89c";
        string groupID = "4c580bda-19ef-4aab-8d75-7c636a7d62f6";
        string[] defaultOrderedColumn = new string[] { "Division", "Name", "Role", "Shift Time", "Pager", "Contact No", "Action" };

        [HttpGet]
        public ActionResult Index(string selDate)
        {
            //Response.Write("Testing:" + Request.Url.ToString());
            var AMiONShiftDetailList = new List<AssignmentDetails>();
            var IsAdmin = true;
            var selectedDate = string.IsNullOrWhiteSpace(selDate) ? DateTime.Now : DateTime.ParseExact(selDate, "yyyy-MM-dd", CultureInfo.InvariantCulture);


            var (adminTabConfigurationDetails, OrderedColumnsAfterRemoved, EditColumns) = GetTabConfigurationDetails(IsAdmin);
            ViewBag.orderedColumns = OrderedColumnsAfterRemoved;
            ViewBag.EditColumns = EditColumns;

            MemoryCache cache = MemoryCache.Default;
            if (!(selectedDate.Date == DateTime.Now.Date && cache.Contains(adminTabConfigurationDetails.Login) && cache[adminTabConfigurationDetails.Login] is List<AssignmentDetails>))
            {

                var BaseAdress = string.Format("http://www.amion.com/cgi-bin/ocs?Lo={0}&Rpt=625btabs--&Month={1}&Day={2}&Year={3}&Days=1", adminTabConfigurationDetails.Login, selectedDate.Month, selectedDate.Day, selectedDate.Year);

                var stream = RestHelper.GetDataFromUrl(BaseAdress).GetAwaiter().GetResult() ?? throw new Exception();
                var ShiftDetailsDataTable = ConvertStreamDataToDataTable(stream);
                AMiONShiftDetailList = ConvertShiftDetailsDataTableToList(ShiftDetailsDataTable);

                if (selectedDate.Date == DateTime.Now.Date)
                {
                    CacheItem cacheItem = new CacheItem(adminTabConfigurationDetails.Login, AMiONShiftDetailList);
                    cache.Add(cacheItem, new CacheItemPolicy() { AbsoluteExpiration = DateTimeOffset.Now.AddMinutes(30) });
                }
            }
            else
                AMiONShiftDetailList = cache[adminTabConfigurationDetails.Login] as List<AssignmentDetails>;

            ViewBag.DepartmentList = AMiONShiftDetailList.Select(x => x.Division).Distinct();
            return View(AMiONShiftDetailList);
        }

        [HttpPost]
        public ActionResult SaveEditColumns(EditColumnsModel editColumnModel, bool isAdmin)
        {

            if (isAdmin == true)
            {
                var adminTabConfigurationDetails = new AMiONTabSetting().GetAdminTabConfigurationDetails(TeamId, ChannelId);
                if (adminTabConfigurationDetails is AdminTabConfigurationDetails)
                {
                    adminTabConfigurationDetails.ExcludedColumns = editColumnModel.RemovedColumns ?? new string[] { };
                    adminTabConfigurationDetails.LastModifiedBy = Guid.Parse(UserID);
                    adminTabConfigurationDetails.LastModifiedDate = DateTime.Now;
                    new AMiONTabSetting().InsertOrUpdateAdminTabConfigurationDetailCollection(adminTabConfigurationDetails);
                }
            }

            var userSetting = new AMiONTabSetting().GetUserSetting(UserID, TeamId, ChannelId);
            if (userSetting is UserSetting)
            {
                userSetting.ColumnsOrder = editColumnModel.OrderedColumns ?? new string[] { };
                new AMiONTabSetting().InsertOrUpdateUserSetting(userSetting);
            }
            else
            {
                UserSetting _userSetting = new UserSetting();
                _userSetting.ColumnsOrder = editColumnModel.OrderedColumns ?? new string[] { };
                _userSetting.CreationDate = DateTime.Now;
                _userSetting.UserId = Guid.Parse(UserID);
                _userSetting.TeamId = Guid.Parse(TeamId);
                _userSetting.ChannelId = ChannelId;
                _userSetting.TenantId = Guid.Parse(TenantId);
                new AMiONTabSetting().InsertOrUpdateUserSetting(userSetting);
            }

            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        private List<AssignmentDetails> ConvertShiftDetailsDataTableToList(DataTable ShiftDetailsDataTable)
        {
            var AMiONShiftDetailList = new List<AssignmentDetails>();

            string GetColumnValueIfExist(DataRow rec, string columnName) => rec.Table.Columns.Contains(columnName) ? rec.Field<string>(columnName) ?? string.Empty : string.Empty;
            string GetAMiONTimeFormat(string timeString)
            {
                if (string.IsNullOrWhiteSpace(timeString))
                    return string.Empty;
                return DateTime.ParseExact(timeString.PadLeft(4, '0'), "HHmm", CultureInfo.InvariantCulture).ToString("h:m tt").Replace(":0", string.Empty).ToLower();
            }

            AMiONShiftDetailList.AddRange(ShiftDetailsDataTable.AsEnumerable().Select(rec => new AssignmentDetails()
            {
                Division = GetColumnValueIfExist(rec, "Division"),
                Role = GetColumnValueIfExist(rec, "Shift_Name"),
                Name = GetColumnValueIfExist(rec, "Staff_Name"),
                ShiftTime = GetAMiONTimeFormat(GetColumnValueIfExist(rec, "Start_Time")) + " - " + GetAMiONTimeFormat(GetColumnValueIfExist(rec, "End_Time")),
                Pager = GetColumnValueIfExist(rec, "Pager"),
                Contact = GetColumnValueIfExist(rec, "Tel"),
                EMailId = GetColumnValueIfExist(rec, "Email"),
                ShiftStart = GetColumnValueIfExist(rec, "Start_Time").PadLeft(4, '0'),
                ShiftEnd = GetColumnValueIfExist(rec, "End_Time").PadLeft(4, '0')
            }));
            return AMiONShiftDetailList;
        }

        private static DataTable ConvertStreamDataToDataTable(Stream stream)
        {
            var ShiftDetailsDataTable = new DataTable();
            var isHeader = true;
            using (StreamReader sr = new StreamReader(stream))
            {
                string Row = String.Empty;
                while ((Row = sr.ReadLine()) != null)
                {
                    if (isHeader)
                    {
                        ShiftDetailsDataTable.Columns.AddRange(columns: Row.Split('\t').Where(x => !string.IsNullOrWhiteSpace(x)).Select(x => new DataColumn(x, typeof(string))).ToArray());
                        isHeader = false;
                        continue;
                    }
                    ShiftDetailsDataTable.Rows.Add(Row.Split('\t'));
                }
            }

            return ShiftDetailsDataTable;
        }


        private (AdminTabConfigurationDetails adminTabConfigurationDetails, IEnumerable<string> OrderedColumnsAfterRemoved, string EditColumns) GetTabConfigurationDetails(bool IsAdmin)
        {
            var OrderedColumns = defaultOrderedColumn;
            var ExcludedColumns = new string[] { };
            var adminTabConfigurationDetails = new AMiONTabSetting().GetAdminTabConfigurationDetails(TeamId, ChannelId);
            if (adminTabConfigurationDetails is AdminTabConfigurationDetails)
            {
                ExcludedColumns = adminTabConfigurationDetails.ExcludedColumns ?? new string[] { };
                OrderedColumns = adminTabConfigurationDetails.DefaultColumnOrder ?? defaultOrderedColumn;
            }

            var userSetting = new AMiONTabSetting().GetUserSetting(UserID, TeamId, ChannelId);
            if (userSetting is UserSetting)
            {
                if (userSetting.ColumnsOrder != null && userSetting.ColumnsOrder.Length > 0)
                {
                    var accessColumn = OrderedColumns.Except(ExcludedColumns);
                    OrderedColumns = (userSetting.ColumnsOrder.Intersect(accessColumn)).Union(accessColumn).Distinct().ToArray();
                }
            }

            var OrderedColumnsAfterRemoved = OrderedColumns.Except(ExcludedColumns);
            var EditColumns = JsonConvert.SerializeObject(new { UpdatedOrderShiftColumnNames = OrderedColumnsAfterRemoved.ToArray(), RemovedShiftColumnNames = ExcludedColumns.ToArray(), IsAdmin = IsAdmin });

            return (adminTabConfigurationDetails, OrderedColumnsAfterRemoved, EditColumns);
        }

    }
}
