﻿using AMiON.Helper;
using AMiON.Helper.CosmosDB;
using AMiONGraphShift.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace AMiON.ShiftsMapping.Controllers
{
    public class AmionController: Controller 
    {
        string token = string.Empty;

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }


        [HttpGet]
        [Route("AmionData/{amionLogin}")]
        public JsonResult AmionData(string amionLogin)

        {
            var amionData = FetchAmionData(amionLogin);

            if (amionData != null) {
                var uniqueDepartments = amionData.Select(department => department.Division).Distinct();
                List<DepartmentModel> lstDepartmentModel = new List<DepartmentModel>();

                foreach (string departmentName in uniqueDepartments)
                {
                    DepartmentModel departmentModel = new DepartmentModel();
                    departmentModel.DepartmentName = departmentName;
                    departmentModel.ShiftsCount = amionData.FindAll(depart => depart.Division == departmentName).Count;
                    lstDepartmentModel.Add(departmentModel);
                }
                return Json(lstDepartmentModel, JsonRequestBehavior.AllowGet);
            }
            return null;
        }

        private static List<AssignmentDetails> FetchAmionData(string loginData)
        {
            if (loginData == null)
            {
                throw new ArgumentNullException(nameof(loginData));
            }
            return Text2Json.JsonDataFromWebExcel(loginData)
                   ?? null;
        }

        [HttpPost]
        [Route("SaveFile")]
        public ActionResult UploadFiles()
        {
            // Checking no of files injected in Request object  
            if (Request.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                        //string filename = Path.GetFileName(Request.Files[i].FileName);  

                        HttpPostedFileBase file = files[i];
                        string fname;

                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }

                        

                        string path = Server.MapPath("~/Uploads/");

                        if(!(Path.GetExtension(path+fname) == ".xlsx" || Path.GetExtension(path+fname) == ".xls"))
                        {
                            return Json(new { message = "Unsupported format of mapping file...", success = false });
                        }

                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        if (System.IO.File.Exists(path+fname))
                        {
                            // deletevprevious image
                            System.IO.File.Delete(path+fname);
                        }
                        // Get the complete folder path and store the file inside it.  
                        fname = Path.Combine(Server.MapPath("~/Uploads/"), fname);
                        file.SaveAs(fname);
                    }
                    // Returns message that successfully uploaded  
                    return Json(new { message = "File Uploaded Successfully!", success=true });
                }
                catch (Exception ex)
                {
                    return Json(new { message = "Error occurred.Error details: ", success = false });
                }
            }
            else
            {
                return Json(new { message = "No files selected.", success = false });
            }
        }

        [HttpPost]
        [Route("CreateShifts/")]
        public JsonResult ProcessShifts(ImportUserInputModel importUserInputData)
        {
            if (importUserInputData != null)
            {
                var token = importUserInputData.AccessToken;
                string TeamId = "d442bd60-005e-4801-b5c4-d01b2c71c30f";
                string ChannelId = "19:5e22d9e2b79842aab19ffc0202809061@thread.skype";
                string UserID = "3ef0ebf1-7c44-4952-897c-f7725687273a";
                var adminTabConfigurationDetails = new AMiONTabSetting().GetAdminTabConfigurationDetails(TeamId, ChannelId);
                if (adminTabConfigurationDetails is AdminTabConfigurationDetails)
                {
                    adminTabConfigurationDetails.DefaultColumnOrder = new string[] { "Division", "Name", "Role", "Shift Time", "Pager", "Contact No", "Action" };
                    adminTabConfigurationDetails.Login = importUserInputData.AmionLogin;
                    adminTabConfigurationDetails.SelectedDepartments = importUserInputData.SelectedDepartments?.ToArray();
                   // adminTabConfigurationDetails.MappingFilePath = ;
                    adminTabConfigurationDetails.LastModifiedBy = Guid.Parse(UserID);
                    adminTabConfigurationDetails.LastModifiedDate = DateTime.Now;
                    new AMiONTabSetting().InsertOrUpdateAdminTabConfigurationDetailCollection(adminTabConfigurationDetails);
                }
                else
                {
                    adminTabConfigurationDetails.DefaultColumnOrder = new string[] { "Division", "Name", "Role", "Shift Time", "Pager", "Contact No", "Action" };
                    adminTabConfigurationDetails.Login = importUserInputData.AmionLogin;
                    adminTabConfigurationDetails.SelectedDepartments = importUserInputData.SelectedDepartments?.ToArray();
                    // adminTabConfigurationDetails.MappingFilePath = ;
                    adminTabConfigurationDetails.CreationDate = DateTime.Now;
                    adminTabConfigurationDetails.TeamId = Guid.Parse( TeamId);
                    adminTabConfigurationDetails.ChannelId= ChannelId;
                    adminTabConfigurationDetails.LastModifiedBy = Guid.Parse(UserID);
                    adminTabConfigurationDetails.LastModifiedDate = DateTime.Now;
                    new AMiONTabSetting().InsertOrUpdateAdminTabConfigurationDetailCollection(adminTabConfigurationDetails);
                }

                // var token = Helper.Authentication.GetToken();
                var workingThread = new Thread(() => OperationToCallAsync(token, importUserInputData));
                workingThread.Start();

                return Json(new { success = true });
            }
            return Json(new { errorMessage = "No input data passed", errorCode = "0", success = false });
        }

        void OperationToCallAsync(string token, ImportUserInputModel importUserInputData)
        {
            importUserInputData.StartDate = DateTime.Now;
            importUserInputData.EndDate = DateTime.Now.AddDays(1);
            ShiftsProcessor.ShiftCreationProcessor(token, importUserInputData);
        }

      
    }
}