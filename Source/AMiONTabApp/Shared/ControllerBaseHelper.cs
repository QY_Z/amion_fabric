﻿using Newtonsoft.Json;
using System;
using System.Web.Mvc;

namespace AMiONGraphShift.Shared
{
    public class ControllerBaseHelper : Controller
    {

        public TeamContextDetails TeamContextDetails
        {
            get
            {

                var cookieValue = Request.Cookies["TeamContextDetails"].Value;
                if (!string.IsNullOrWhiteSpace(cookieValue))
                    return JsonConvert.DeserializeObject<TeamContextDetails>(cookieValue);

                return null;
            }
        }

        public string AuthToken
        {
            get
            {
                var cookie = HttpContext.Request.Cookies.Get("amion_");
                var accessToken = cookie?.Value;
                if (string.IsNullOrWhiteSpace(accessToken))
                {
                    string baseAddress = HttpContext.Request.Url.GetLeftPart(UriPartial.Authority);
                    Redirect($"{baseAddress}/AmionAuth.html");
                }

                return accessToken;
            }
        }
    }

    public class TeamContextDetails
    {
        public Guid TeamId { get; set; }
        public string ChannelId { get; set; }
        public Guid tenantId { get; set; }
        public Guid UserId { get; set; }
    }
}