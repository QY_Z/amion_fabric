﻿import React from 'react';

export class Shifts_index extends React.Component {
    render() {
        return (
            <div ClassName="ImportShiftDiv ms-Fabric ms-bgColor-gray20" style={{ fontFamily:'Segoe UI' }} >
                <div className="navbar" id="MainNavBar">
                    <Navbar />
                </div>
                <div id="divEditColumnModel" style={{ width: '100%', background: 'white', position:'absolute', zIndex:'1',display:'none'}} ClassName="one-edge-shadow">
                    <DivEditColumnModel />
                </div>
                <div id="ChildMainDiv" style={{ padding: '7px 5px', minHeight:'48px',display:'none'}} ClassName="navbar">
                    <ChildMainDiv />
                </div>
                <div>
                    <Table />
                </div>
            </div>
        );
    }
}

