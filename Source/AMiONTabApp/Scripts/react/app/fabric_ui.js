﻿import React from 'react';
import { Toggle } from 'office-ui-fabric-react/lib/Toggle';
import { SearchBox } from 'office-ui-fabric-react/lib/SearchBox';
import { initializeIcons } from 'office-ui-fabric-react/lib/Icons';


initializeIcons();

export class Fabric_ui extends React.Component {

    render() {
        return (
            <div className="ImportShiftDiv ms-bgColor-gray20" style={{ fontFamily: 'Segoe UI' }}>
                <div className="navbar" id="MainNavBar">
                    <div className="pull-left" id="MainLeftDiv">
                        <div className="btn-group pull-left">
                            <button className="btn SelDepartment" type="button" data-toggle="dropdown" title="Select a Team">
                                <span id="SpanDepartment">All Groups</span>
                                <span style={{ top: '5px', fontSize: 'xx-small' }}><i className="ms-Icon ms-Icon--ChevronDown"></i></span>
                            </button>
                            <ul className="dropdown-menu SelDepartmentOption ms-md6" style={{minWidth:'219px'}}>
                                <li className="SelectDepartment padding5px OnClickSelectDepartment" >
                                    <div>
                                        <input className="styled-checkbox " id="chkAllGroups" type="checkbox"/>
                                        <label for="chkAllGroups">All new Groups</label>
                                    </div>
                                </li>



                                <li className="SelectDepartment padding5px OnClickSelectDepartment" >
                                <div>
                                    <input className="styled-checkbox " id="chkAllGroups-@(i)" type="checkbox"/>
                                        <label for="">Department</label>
                                </div>
                            </li>




                            </ul>
                        </div>
                        <input type='button' readonly="readonly" className="txtDatetimePicker btn pull-right" id="datetimepicker" value="" />
                    </div>
                    <div className="pull-right" style={{ width:'30px' }}>
                        <a className="EditColumnIcon" data-toggle="dropdown"><i title="Edit column" className="ms-Icon ms-Icon--More"></i></a>
                        <ul className="dropdown-menu" style={{ width: '15em', right: '0', left:'auto' }}>
                            <li className="padding5px ShowEditColumnModel">
                                <div>
                                    <label>  <a className="glyphicon glyphicon-pencil"></a> <span style={{ cursor:'pointer' }}>Edit Columns</span></label>
                                </div>
                            </li>
                            <li className="padding5px">
                                <div>
                                    <label>  <a className="glyphicon glyphicon-question-sign"></a> <span style={{ cursor: 'pointer' }}>Help</span></label>
                                </div>
                            </li>
                        </ul>

                    </div>
                    <div className=" pull-right" id="MainRightDiv">
                        <div className="ui small icon input pull-right">
                            <SearchBox
                                styles={{ root: { width: 200,border:0}}}
                                placeholder="Search"
                                onChange={(_, newValue) => console.log('SearchBox onChange fired: OnKeyUpSearchShiftDetails' + newValue)}
                                onBlur={() => console.log('onBlur OnblurSearchShiftBlur')}
                            />
                            <input type="text" className="InputSearchShiftDetails OnKeyUpSearchShiftDetails OnblurSearchShiftBlur" placeholder="Search..."/>
                                <i className="search icon "></i>
                        </div>
                        <div className="ShowSwitchIfToday">
                            <Toggle
                                label="Inactive Shifts"
                                inlineLabel={true}
                                className="chkInactiveStatus OnclickShowHideInactiveShiftDetails"
                                defaultChecked={false}
                                onFocus={() => console.log('onFocus called')}
                                onBlur={() => console.log('onBlur called')}
                            />
                            <label className="switch">
                                <input type="checkbox" className="chkInactiveStatus OnclickShowHideInactiveShiftDetails" />
                                <span className="slider round"></span>
                            </label>
                            <span>Inactive Shifts</span>
                        </div>
                    </div>
                    <input type="hidden" value="" id="hdnEditColumns" />
                </div>
            </div>

            
        );
    }
}