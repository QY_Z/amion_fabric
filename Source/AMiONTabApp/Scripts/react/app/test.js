﻿import React from 'react';

export class LoggingButton extends React.Component {
    render() {
        return (
            <div className="ImportShiftDiv ms-bgColor-gray20" style={{ fontFamily: 'Segoe UI' }}>
                <div class="navbar" id="MainNavBar">
                    <div className="pull-left" id="MainLeftDiv">
                        <div className="btn-group pull-left">
                            <button className="btn SelDepartment" type="button" data-toggle="dropdown" title="Select a Team">
                                <span id="SpanDepartment">All Groups</span>
                                <i className="ms-Icon ms-Icon--ChevronDown" style={{ top: '5px', FontSizes: 'xx-small' }}></i>
                            </button>
                            <ul className="dropdown-menu SelDepartmentOption ms-md6" style={{ minWidth: '219px' }}>
                                <li className="SelectDepartment padding5px OnClickSelectDepartment" >
                                    <div>
                                        <input className="styled-checkbox " id="chkAllGroups" type="checkbox" />
                                        <label for="chkAllGroups">All Groups</label>
                                    </div>
                                </li>



                                <li className="SelectDepartment padding5px OnClickSelectDepartment" >
                                    <div>
                                        <input className="styled-checkbox " id="chkAllGroups-@(i)" type="checkbox" />
                                        <label for="">Department</label>
                                    </div>
                                </li>




                            </ul>
                        </div>
                        <input type='button' readonly="readonly" className="txtDatetimePicker btn pull-right" id="datetimepicker" value="" />
                    </div>
                </div>
            </div>
             
        );

    }
}