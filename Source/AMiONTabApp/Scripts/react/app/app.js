﻿import React from 'react';
import ReactDOM from 'react-dom';
import { Fabric_ui } from './fabric_ui.js';

ReactDOM.render(
    <Fabric_ui />,
    document.getElementById('root')
);

