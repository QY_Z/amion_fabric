﻿module.exports = {
    context: __dirname,
    entry: "./app/app.js",
    output: {
        path: __dirname + "/dist",
        filename: "bundle.js"
    },
    mode: 'production',
    watch: true,
    module: {
        rules: [
            {
                test: /(\.jsx|\.js)$/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: [
                            "babel-preset-env", "babel-preset-react"
                        ]
                    }
                },
                exclude: /node_modules/
            }
        ]
    }
};
