﻿using System.Web.Http;


namespace AMiONGraphShift
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.EnableCors();
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{selDate}",
                defaults: new { selDate = RouteParameter.Optional}
            );

        }
    }
}